TillLunch: fetch lunch menus
=============================

This simple Python program fetches from the web information about
lunch menus in a few restaurants in the area around the campus of
Chalmers University of Technology in Gothenburg, Sweden.

Installation and dependencies
-----------------------------

TillLunch uses some Python libraries that can be installed using `pip`:

```bash
$ pip install beautifulsoup4 selenium
```

While [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) works out of the box, [Selenium](http://selenium-python.readthedocs.io) has a few more dependencies:

   1. Make sure a supported browser is installed (I use Firefox).
   
   2. Install a suitable driver (for
      [Firefox](https://github.com/mozilla/geckodriver/releases)), by
      downloading the binary for your architecure and putting it in a
      directory in the system's path (I use `/usr/local/bin`).

TillLunch uses [CutyCapt](http://cutycapt.sourceforge.net/) to perform
simple Facebook scraping, and `pdftotext` to convert PDF
sources. Install CutyCapt with:

```bash
$ apt install cutycapt
```

whereas `pdftotext` should be available in most Linux distributions.

Finally, to install TillLunch copy or link `tillunch.py` to a
directory in the path, and add it to executable permissions.


Usage examples
--------------

The command-line help, accessible with the `-h` option, should be self-explanatory. 

A few examples:

   * Fetch today's menus of all available restaurants:

      ```
	  $ tillunch.py
      ```
	 
   * Fetch the Wednesday menu of restaurants OOTO and Einstein:

    ```
	 $ tillunch.py -d 2 ooto einstein
	 ```
	 
   * Fetch today's menus of all available restaurants *but* Mr. P,
     showing the URLs of the fetched pages:

     ```
	 $ tillunch.py -v --url mrp
	 ```


Environment
------------

TillLunch has been written and tested under Ubuntu 16.04 with Python
2.7. Your mileage may vary.
