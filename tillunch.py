#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2017 Carlo A. Furia

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


all_restaurants = ["ooto", "einstein", "wijkanders", "bistro", "mrP", "linsen", "indianBBQ", "thai"]
ALL = "ALL"

out_formats = ["full", "compact"]

import urllib2
from bs4 import BeautifulSoup
import os
import sys, argparse


days = {"english":
        [u"monday", u"tuesday", u"wednesday", u"thursday", u"friday", u"saturday", u"sunday"],
        "swedish":
        [u"måndag", u"tisdag", u"onsdag", u"torsdag", u"fredag", u"lördag", u"söndag"]}


def today():
    import datetime
    return datetime.datetime.today().weekday()


def souped(url):
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page, "html.parser")
    return soup


def wijkanders(on_day = today()):
    url = "http://www.wijkanders.se/restaurangen/"
    soup = souped(url)
    for day in soup.find_all("strong"):
        if day.text.startswith(days["swedish"][on_day].upper()):
            return (url, day.parent.text.rstrip().lstrip())


def einstein(on_day = today()):
    url = "http://www.butlercatering.se/einstein"
    soup = souped(url)
    for day in soup.find_all("h3", attrs = {"class": "field-label"}):
        if day.text.startswith(days["swedish"][on_day].title()):
            return (url, day.parent.text.rstrip().rstrip())


def ooto(on_day = today()):
    url = "http://ooto.se/sv/lunchmeny/"
    soup = souped(url)
    on_today = days["swedish"][on_day].upper()
    for day in soup.find_all("h2", attrs = {"class": "av-special-heading-tag",
                                            "itemprop": "headline"}):
        
        if day.text.startswith(on_today):
            block_menu, idx = [], -1
            begin_at = 0
            for el in day.parent.parent.parent.children:
                try:
                    block_menu += [el.text]
                    idx += 1
                    if el.text.startswith(on_today):
                        begin_at = idx
                except AttributeError:
                    pass
            text_menu = "\n".join(block_menu[begin_at:begin_at + 4])
            return (url, os.linesep.join([s for s in text_menu.splitlines() if s]))


def mrP(on_day = today()):
    url = "http://www.mr-p.se/lunch/"
    soup = souped(url)
    for day in soup.find_all("h3"):
        if day.text.startswith(days["swedish"][on_day].title()):
            break
    menu = [day]
    for i in range(5):
        menu += [menu[-1].nextSibling]
    for veg in soup.find_all("h3"):
        if veg.text.startswith("Veckans vegetariska"):
            menu += [veg, veg.nextSibling, veg.nextSibling.nextSibling]
    text_menu = "\n".join([e.text for e in menu if e != u"\n"])
    return (url, text_menu)


def thai(on_day = today()):
    url = "http://arojjdii2.kvartersmenyn.se/"
    soup = souped(url)
    menu = soup.find("div", attrs = {"class": "meny"})
    return (url, menu.get_text(separator = "\n"))


def _find_post_url(base_url, on_day):
    import subprocess, tempfile, re
    outfile = next(tempfile._get_candidate_names())
    scraped = True
    try:
        subprocess.check_call(["cutycapt",
                               "--url=" + base_url,
                               "--out=" + outfile,
                               "--out-format=html"])
    except CalledProcessError:
        scraped = False
    base = base_url[[i for i, letter in enumerate(base_url) if letter == "/"][-3]:]
    if scraped:
        pids = []
        with open(outfile, "r") as fp:
            soup = BeautifulSoup(fp, "html.parser")
            for day in soup.find_all("a", attrs = {"class": "see_more_link"}):
                if "href" in day.attrs:
                    match = re.search("^" + base + "([0-9]+)$", day.attrs["href"])
                    if match is not None:
                        pids += [match.group(1)]
    else:
        pids = []
    try:
        os.remove(outfile)
    except OSError:
        pass
    on_today = today()
    if on_day <= on_today:
        offset = on_today - on_day  # today == first match == latest post
    else:
        offset = on_today + (6 - on_day + 1)  # on_day last week
    try:
        return pids[offset]
    except:
        return None

    
def bistro(on_day = today()):
    import subprocess, tempfile
    fetch_url = "https://www.facebook.com/pg/timeoutbycharlie/posts/"
    base_url = "https://www.facebook.com/timeoutbycharlie/posts/"
    todays_pid = _find_post_url(fetch_url, on_day)
    if todays_pid is None:
        return (None, None)
    url = base_url + todays_pid
    outfile = next(tempfile._get_candidate_names())
    scraped = True
    try:
        subprocess.check_call(["cutycapt",
                               "--url=" + url,
                               "--out=" + outfile,
                               "--out-format=html"])
    except CalledProcessError:
        scraped = False
    if scraped:
        with open(outfile, 'r') as fp:
            soup = BeautifulSoup(fp, "html.parser")
            for day in soup.find_all("p"):
                if day.text.startswith(days["swedish"][on_day].title()):
                    menu = day.parent.get_text(separator = "\n")
    else:
        menu = None
    try:
        os.remove(outfile)
    except OSError:
        pass
    return (url, menu)


def linsen(on_day = today()):
    url = "http://www.cafelinsen.se/menyer/cafe-linsen-lunch-meny.pdf"
    import subprocess, tempfile, re
    pdf_file = next(tempfile._get_candidate_names())
    with open(pdf_file, 'wb') as f:
        f.write(urllib2.urlopen(url).read())
        f.close()
    try:
        menu = subprocess.check_output(["pdftotext", pdf_file, "-"]).decode("utf-8")
    except CalledProcessError:
        menu = None
    try:
        os.remove(pdf_file)
    except OSError:
        pass
    menu_lines = [el.strip() for el in menu.splitlines()]
    idx_today = menu_lines.index(days["swedish"][on_day].title())
    idx_weekly = [i for i in range(len(menu_lines)) if menu_lines[i].startswith(u"Stående rätter")][0]
    menu = menu_lines[idx_today:idx_today+4] + menu_lines[idx_weekly:-1]
    return (url, "\n".join(menu))


def indianBBQ(on_day = today()):
    url = "http://www.indianbarbeque.se/lunch-1"
    from selenium import webdriver
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.webdriver.common.by import By
    from selenium.common.exceptions import TimeoutException    
    browser = webdriver.Firefox() # launch an instance of Firefox
    browser.get(url) # navigate to url
    delay = 3
    try:  # wait until id SITE_CONTAINER has been loaded into the page
        page = WebDriverWait(browser,
                             delay).until(EC.presence_of_element_located((By.ID, 'SITE_CONTAINER')))
    except TimeoutException:
        page = None
    innerHTML = browser.execute_script("return document.body.innerHTML") # get rendered HTML
    browser.close() # close browser instance
    soup = BeautifulSoup(innerHTML, "html.parser")
    for day in soup.find_all("span"):
        if day.text.startswith(days["swedish"][on_day].title()):
            break
    menu = [day.parent]
    for i in range(2*2*3):
        try:
            menu += [menu[-1].nextSibling]
        except AttributeError:
            break
    text_menu = "\n".join([e.text for e in menu if e is not None and e != u"\n"])
    return (url, text_menu)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Fetches the daily menus in a few restaurants around Chalmers.")
    parser.add_argument("-d", "--day", dest="day", default=today(), type=int, choices=range(7),
                        help="day of week to fetch the menus for (default: %(default)s), " + \
                             "numbered from monday")
    parser.add_argument("-u", "--url", dest="url", action="store_true",
                        help="add urls of fetched pages")
    parser.add_argument("-v", "--invert", dest="inv", action="store_true",
                        help="the listed restaurants are to be excluded from the search")
    parser.add_argument("-f", "--format", dest="fmt", choices=out_formats, default="full",
                        help="output formatting (default: %(default)s)")
    parser.add_argument("--debug", dest="debug", action="store_true",
                        help="print debug information if something goes wrong")
    parser.add_argument("rests", nargs="*", default = ALL, 
                        choices = [ALL] + all_restaurants,
                        help="restaurants to be included in the search (default: %s)" % ALL)
    args = parser.parse_args()
    if type(args.rests) is list:
        restaurants = args.rests
    elif type(args.rests) is str and args.rests == ALL:
        restaurants = all_restaurants
    if args.inv:
        restaurants = [r for r in all_restaurants if r not in restaurants]
    this_module = sys.modules[__name__]
    menus = []
    if args.fmt == "full":
        SEP, PRE = "-----------------", ""
    elif args.fmt == "compact":
        SEP, PRE = ":", "\n\n"
    for r in restaurants:
        menus += [PRE + r.upper(), SEP]
        try:
            fetcher = getattr(this_module, r)
            url, menu = fetcher(on_day = args.day)
            if args.url:
                menus += [url]
            if menu is not None:
                if args.fmt == "full":
                    menu = menu.lstrip().rstrip()
                elif args.fmt == "compact":
                    menu = " ".join([m.strip() for m in menu.splitlines()])
                menus += [menu]
            else:
                menus += ["Could not retrieve a valid menu for today"]
        except Exception as e:
            menus += ["Something went wrong fetching"]
            if args.debug:
                print >>sys.stderr, str(e)
        menus += ["\n"]
    if args.fmt == "full":
        menus = u"\n".join(menus[:-1]).encode("utf-8")
    elif args.fmt == "compact":
        menus = [m.rstrip() for m in menus if m.strip() != ""]
        menus = u" ".join(menus).encode("utf-8")
    print menus
